/* $ works like jQuery and is expected as argument from cheerio */

export const getText = ($, selector) => {
	return $(selector).text();
}

export const elExists = ($, selector) => {
	return ($(selector).length > 0);
}

export const getLinks = ($, selector) => {
	let hrefs = [];
	$(selector).each((i, a) => {
		hrefs.push($(a).attr('href'));
	});
	return hrefs;
}

export const getAttr = ($, selector, attr) => {
	return $(selector).attr(attr);
}