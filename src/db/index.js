import mongodb, {ObjectID} from 'mongodb';
import { dbPath } from '../config/db.js';

const mongoPath = dbPath();
const MongoClient = mongodb.MongoClient;

const LISTING_COLLECTION = "listings"; 

const dbConnect = () => {
	return new Promise((resolve, reject) => {
		MongoClient.connect(mongoPath)
		.then(client => {
			resolve(client);
		}).catch(err => {
			reject(err);
		});
	});
}

export const getLatest = () => {
	const TO_UPDATE_BATCH_COUNT = 15;
	return new Promise((resolve, reject) => {
			dbConnect().then(client => {
				let db = client.db();
				db.collection(LISTING_COLLECTION).find({listingStatus: null}).limit(TO_UPDATE_BATCH_COUNT).toArray((err, r) => {
					if (err) reject(err);
					resolve(r);
					client.close();
				})
			}).catch( err => {
				reject(err);
			});
		}
	);
}

export const findExistentAndInsertNew = (data) => {
	return new Promise((resolve, reject) => {
		dbConnect().then(client => {
			let db = client.db();
			const platform = data[0].platform;
			const parsedCodes = data.map(e => { // all of the parsed codes
				return e.code;
			});

			db.collection(LISTING_COLLECTION).find({ 
				platform, 
				code: { $in: parsedCodes }
			}).toArray((err, r) => {
				let foundCodes = null;				
				if (r === null){
					foundCodes = [] //means it found no codes
				} else {
					foundCodes = r.map(e => e.code); // codes found in db		
				}
				const newListings = data.filter(e => { // removes listings that match codes found in db
					return !foundCodes.includes(e.code);
				});

				if (newListings.length > 0){
					db.collection(LISTING_COLLECTION).insertMany(newListings).then(r => {
						resolve(r);
					}).catch(err => {
						reject(err)
					})
				}

				client.close();
			
			});
		}).catch( err => {
			reject(err);
		});
	});
}

export const updateListing = (_id, data) => {
	return new Promise((resolve, reject) => {
		dbConnect().then(client => {
			const db = client.db();
			db.collection(LISTING_COLLECTION)
			.update({_id: ObjectID(_id)}, {$set: data})
			.then(r => {
				resolve(r);
			}).catch(err => {
				reject(err);
			});
			client.close();
		}).catch(err => {
			reject(err);
		});
	});
}