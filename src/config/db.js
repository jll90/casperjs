import { isProduction } from './env.js';

let username = null; 
let password = null;
let port = null;
let hostOrIP = null; 
let dbName = null;

if (isProduction()){
	username = process.env.APP_ADMIN_USER;
    password = process.env.APP_ADMIN_PASS;
    port = "27017";
	hostOrIP = process.env.APP_HOST_IP;
	dbName = "unnamed";
} else {
    username = "defaultUser";
    password = "12345678";
    port = "27017";
	hostOrIP = "localhost";
	dbName = "unnamed";
}

export const dbPath = () => {
	return `mongodb://${username}:${password}@${hostOrIP}:${port}/${dbName}`;
}