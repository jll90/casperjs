export const isProduction = () => {
	const env = process.env.NODE_ENV;
	return (env === "production");
}