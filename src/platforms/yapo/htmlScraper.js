import casper from 'casper';
import utils from 'utils';

import {
	stripTabs,
	stripCarriageReturn,
    stripLastCharacter
} from '../../helpers/string.js';

import { parsePrice } from '../../helpers/price.js';

import { addToProperty } from '../../helpers/property.js';

const client = casper.create();

client.options.timeout = 60000;

client.options.onTimeout = function(){
    utils.dump({error: "timeout"});
    this.exit();
}

const arg = client.cli.args;
const BASE_URL = "https://www.yapo.cl";
let _id = arg[0];
let url = arg[1];

if (url === undefined){
    url = "https://www.yapo.cl/tarapaca/arrendar/exclusivo_centro_de_belleza_amplio_box_52403076.htm?ca=2_s&first=1&oa=52403076&xsp=0";
} else {
    url = BASE_URL + url;
}

client.start(url);

client.then(() => {
    let property = {};

    const phoneImgUrl = client.getElementAttribute(".da-phone-num", "src");
    property.phoneImgUrl = phoneImgUrl;

    const region = parseStateFromUrl(url);
    property.region = region;

    const operationType = parseOperationTypeFromUrl(url);
    property.operationType = operationType;

    const selectors = {
        title: '#da_subject',
        description: '.description p',
        contactName: '.publisher-info h3',
    }

    for (let key in selectors){
    	let selector = selectors[key];

    	if (client.exists(selector)){
    		const val = client.fetchText(selector);
    		let parsedVal = null;

    		switch(key){
    			default:
    				parsedVal = val;
    				break;
    		}

    		property = addToProperty(property, key, parsedVal);
    	}
    }

    property = parseDetails(property);
    //returns to stdout
    utils.dump(property);

});

client.run();

/* Str -> Str */
const parseStateFromUrl = url => {
    return url.split("/")[3];
}

/* Str -> Str */
const parseOperationTypeFromUrl = url => {
    return url.split("/")[4];
}

/* This function parses the table that contains almost all the property info */
const parseDetails = (property) => {
    const detailsSelector = '.details table';
    const tableText = client.fetchText(detailsSelector);

    let tableArr = tableText.split('\n\t\t'); /* Splits table at headings */
    tableArr = tableArr.filter(v => { /* Removes tabs and unneeded data from array*/
        const filterRegex = /[A-Za-z0-9]/gi;
        if (v.match(filterRegex)){
            return true;
        }
        return false;
    });

    tableArr = tableArr.map(v => { /* Removes padding tabs and carriage returns */
        return stripTabs(stripCarriageReturn(v));
    });

    return tableParser(tableArr, property);
}

/* Arr, Obj -> Obj */
const tableParser = (tableValues, property) => {
    const length = tableValues.length;
    let newProperty = Object.assign({}, property);
    for (let i=0; i<length; i++){
        const value = tableValues[i];
        let parsedVal = null;
        let key = null;

        switch(value){ /* Row headings */
            case "Precio":
                parsedVal = parsePrice(tableValues[i+1]);
                key = "price";
                break;
            case "Dormitorios":
                parsedVal = tableValues[i+1][0];
                key = "bedrooms";
                break;
            case "Baños":
                parsedVal = tableValues[i+1][0];
                key = "bathrooms";
                break;
            case "Tipo de inmueble":
                parsedVal = stripLastCharacter(tableValues[i+1]);
                key = "propertyType";
                break;
            case "Superficie total":
                parsedVal = parseSurface(tableValues[i+1]);
                key = "biggerSurface";
                break;
            case "Superficie construida":
            case "Superficie útil":
                parsedVal = parseSurface(tableValues[i+1]);
                key = "smallerSurface";
                break;
            case "Comuna":
                parsedVal = tableValues[i+1];
                key = "county";
                break;
            case "Estacionamiento":
                parsedVal = tableValues[i+1];
                key = "parkingSpots";
                break;
            case "Gastos comunes":
                parsedVal = parsePrice(tableValues[i+1]);
                key = "utilities";
                break;
        }

        if (parsedVal !== null){
            newProperty = addToProperty(property, key, parsedVal);
        }
    }

    return newProperty;
}

const parseSurface = val => {
    const regex = /\d+/gi;
    const surface = val.match(regex)[0];
    return surface;
}