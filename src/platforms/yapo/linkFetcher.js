import * as db from '../../db/index.js';

import { exec } from 'child_process';

console.log("Running linkFetcher as YAPO");

exec('casperjs /home/dabao/scrapejs/dest/platforms/yapo/linkFetcherCasper.js', (err, stdout, stderr) => {
  if (err) {
    console.error(`exec error: ${err}`);
    return;
  }
  let listings = JSON.parse(stdout);

  if (listings.length > 0){
		/* Verify existing listings */
		/* Inserts new listings */

		db.findExistentAndInsertNew(listings).then(result => {
			console.log(`Inserted ${listings.length} records`);
		}).catch(err => {
			console.error(err);
		});
	}
});