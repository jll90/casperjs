import casper from 'casper';
import utils from 'utils';

let url = 'https://www.yapo.cl/chile/inmuebles?ca=15_s&l=0&st=a';

const client = casper.create();

client.start(url);

client.then(function getLinks(){

	let links = [];

    links = client.evaluate(function(){
        var links = document.querySelectorAll('.listing_thumbs a');
        links = Array.prototype.map.call(links,function(link){
            return link.getAttribute('href');
        });
        return links;
    });

    links = links.filter((elem, pos, arr) => { //removes duplicases
    	return arr.indexOf(elem) == pos;
  	});

  	let codes = links.map(link => {
	    const regex = /_(\d+)\.htm/gi;
	    const matches = regex.exec(link);
	    const code = matches[1];
	    return code;
	});

	let data = codes.map((e, i) => {
		let code = codes[i];
		let url = links[i].substring(19);
		let platform = "YAPO";
		return { code, url, platform };
	});

	utils.dump(data);
});

client.run();