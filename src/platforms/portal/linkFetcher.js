import request from 'request-promise';
import cheerio from 'cheerio';

import { getLinks } from '../../parser/index.js';

import { urlGenerator} from './helpers.js';

import * as db from '../../db/index.js';

const urls = urlGenerator(); 
// Function knows how to determine operation and region based
// on system time
console.log("Running linkFetcher as PORTAL");

const promises = urls.map(url => request(url));

Promise.all(promises).then((data) => {
	let links = [];
    links = data.map(html => {
    	return getLinks(cheerio.load(html), '.products-list a');
    });

    links = links.reduce((acc,arr) => [...acc, ...arr], []); //flattens the array
    
    links = links.filter(link => { //removes Handler links
		if (link.indexOf('Handler') > -1){
			return false;
		}
		return true;
	});

	links = [ ...new Set(links)]; //removes duplicates

	let codes = links.map(link => {
		const regex = /\/(\d+)\-/gi;
	    const matches = regex.exec(link);
	    const code = matches[1];
	    return code;
	});	

	let listings = codes.map((e, i) => {
		let code = codes[i];
		let url = links[i];
		let platform = "PORTAL";
		return { code, url, platform };
	});

	if (listings.length > 0){
		/* Verify existing listings */
		/* Inserts new listings */

		db.findExistentAndInsertNew(listings).then(result => {
			console.log(`Inserted ${listings.length} records`);
		}).catch(err => {
			console.error(err);
		});
	
	}
});