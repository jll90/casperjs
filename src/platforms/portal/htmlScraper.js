import request from 'request';
import cheerio from 'cheerio';

import {
    stripBlanks, 
    stripMultipleSpacesButEndings,
    stripCarriageReturn,
    stripLastCharacter,
    stripFirstCharacter,
    stripDoubleQuotes,
    stripExcessBlanks,
    stripBrackets,
    stripSemiColons,
    stripHtmlBlank
} from '../../helpers/string.js';

import { parsePrice } from '../../helpers/price.js';

import { addToProperty } from '../../helpers/property.js';

import { getText, elExists, getAttr } from '../../parser/index.js';

const arg = process.argv;
const BASE_URL = "https://www.portalinmobiliario.com";
let _id = arg[2];
let url = arg[3];

if (url === undefined){
    url = "https://www.portalinmobiliario.com/arriendo/departamento/las-condes-metropolitana/4027982-don-carloscarmencita-uda";
} else {
    url = BASE_URL + url;
}

request(url, (err, res, html) => {

    let property = {};

    if (err){
        return;
    }
    
    const $ = cheerio.load(html);
    /* Handles 404 */
    if (elExists($, "h1.text-404")){
        console.log(JSON.stringify({listingStatus: false}));
        return;
    }
    
    let latitude = getAttr($, "meta[property='og:latitude']", "content");
    property.latitude = latitude;

    let longitude = getAttr($, "meta[property='og:longitude']", "content");
    property.longitude = longitude;

    /* This needs to be refactored */

    let stripTagsArr = $("script").map((i, x) => {

        if (x.children.length > 0){
            let data = x.children[0].data;
            return data;
        }
        return "";
    });

    const length = stripTagsArr.length;

    let stripTagsText = null;

    for (let j=0; j<length; j++){
        let testStr = stripTagsArr[j];
        if (testStr.indexOf('datosContacto') > -1){
            stripTagsText = testStr;
        }
    }

    if (stripTagsText !== null){
        let contactInfo = parseContactInfoFromScriptTags(stripTagsText);
        for (let key in contactInfo){
            addToProperty(property, key, contactInfo[key]);
        }
    }

    /* End Refactor */

    /* Basic selectors that are easily scrapable */
    const selectors = {
        title: '.property-title h1',
        description: '.propiedad-descr',
        propertyType: '.breadcrumb li:nth-of-type(3) a',
        operationType: '.breadcrumb li:nth-of-type(2) a',
        date: '.small-content-panel .operation-internal-code:nth-of-type(3) strong',
        county: '.breadcrumb li:nth-of-type(5) a',
        region: '.breadcrumb li:nth-of-type(4) a',
        price: '.price',
        approximateLocation: '.data-sheet-column-address p',
        bedrooms: '.data-sheet-column-programm p',
        bathrooms: '.data-sheet-column-programm p',
        privateRooms: '.data-sheet-column-programm p',
        smallSurface: '.data-sheet-column-area p',
        bigSurface: '.data-sheet-column-area p',
        brokerName: '.operation-contact-name'
    }

    for (let key in selectors){

        let selector = selectors[key];

        if (elExists($, selector)){            
            const val = getText($, selector); //fetches text from each html element
            let parsedVal = null;
            switch(key){
                case 'description':
                    parsedVal = parseDescription(val);
                    break;
                case 'price':
                    parsedVal = parsePrice(val);
                    break;
                case 'approximateLocation':
                    parsedVal = parseApproximateLocation(val);
                    break;
                case 'bedrooms':
                    parsedVal = parseBedrooms(val);
                    break;
                case 'bathrooms':
                    parsedVal = parseBathrooms(val);
                    break;
                case 'privateRooms':
                    parsedVal = parsePrivateRooms(val);
                    break;
                case 'smallSurface':
                    parsedVal = parseSmallSurface(val);
                    break;
                case 'bigSurface':
                    parsedVal = parseBigSurface(val);
                    break;
                case 'date':
                    parsedVal = parseDate(val);
                    break;
                default: 
                    parsedVal = val;
                    break;
            }
            property = addToProperty(property, key, parsedVal); //adds it to object
        }        
    }

    console.log(JSON.stringify(property));
    
});

/* To rescue the oldest version see github history */

const parseContactInfoFromScriptTags = str => {
    let contactName = null;
    let phoneNumber = null;
    let email = null;

    let newStr = stripCarriageReturn(str);
    const regex = /\{.*(nombreVendedor|telefonosVendedor|emailVendedor).*\};/gi;
    const matches = newStr.match(regex);

    const matchedStr = matches[0];

    let components = matchedStr.split(',');
    const length = components.length;

    for (let i=0; i < length; i++){
        let component = components[i];

        if (component.indexOf("nombreVendedor") > -1){
            contactName = trimContactData(component.split(";")[1]);
        }

        if (component.indexOf("telefonosVendedor") > -1){
            phoneNumber = trimContactData(component);
        }

        if (component.indexOf("emailVendedor") > -1){
            email = trimContactData(component);
        }
    }

    return { contactName, phoneNumber, email };
}

const trimContactData = str => {
    //format heading : data
    let newStr = str.split(":")[1];
    return stripHtmlBlank(stripExcessBlanks(stripBrackets(stripSemiColons(stripDoubleQuotes(newStr)))));
}


const parseDate = str => {
    return str.split(" ")[1];
}

/* Str -> Str */
const parseBigSurface = str => {
    const regex = /\d+/g;
    const matches = str.match(regex);
    const length = matches.length;

    switch(length){
        case 2:
            return matches[1];
        case 1:
            if ( (str.indexOf('terreno') > -1) || (str.indexOf('total') > -1)){
                return matches[1];
            }
            return null;
        default:
            return null;
    }
}

/* Str -> Str */
const parseSmallSurface = str => {
    const regex = /\d+/g;
    const matches = str.match(regex);
    const length = matches.length;

    switch(length){
        case 2:
            return matches[0];
        case 1:
            if ( (str.indexOf('construida') > -1) || (str.indexOf('útil') > -1)){
                return matches[1];
            }
            return null;
        default:
            return null;
    }
}

/* Str -> Str */
const parseBedrooms = str => {
    if (str.indexOf("Dormitorio") > -1){
        return str[0];
    }
    return null;
}

/* Str -> Str */
const parseBathrooms = str => {
    if (hasBathroomsAndMore(str)){
        if (str.indexOf("Baños") > -1){
            return str.slice(-7)[0];
        }

        if (str.indexOf("Baño") > -1){
            return str.slice(-6)[0];
        }
    }

    if (str.indexOf("Baño") > -1){
        return str[0];
    }
    
    return null;
}

/* Str -> Str */
const parsePrivateRooms = str => {
    if (str.indexOf("Privado") > -1){
        return str[0];
    }
    return null;
}

/* Str -> Str */
const parseCodeFromUrl = url => {
    const regex = /\/(\d+)\-/gi;
    const matches = regex.exec(url);
    const code = matches[1];
    return code;
}

/* Str -> Str */
const parseDescription = str => {
    return stripMultipleSpacesButEndings(str);
}

/* Str -> Str */
const parseApproximateLocation = str => {
    return stripMultipleSpacesButEndings(str);
}

/* Str -> Str */
const parsePropertyTypeFromUrl = url => {
    return url.split("/")[4];
}

/* Str -> Str */
const parseOperationFromUrl = url => {
    return url.split("/")[3];
}

/* Str -> Str */
const hasBathroomsAndMore = str => {
    return (str.length > 6);
}