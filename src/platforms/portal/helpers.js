export const urlGenerator = () => {	/* name reference as they come fron the command line */
	const propertyTypes = [
		"casa", 
		"departamento", 
		"oficina", 
		"sitio", 
		"comercial", 
		"industrial", 
		"agricola", 
		"loteo", 
		"bodega", 
		"parcela", 
		"estacionamiento", 
		"terreno-en-construccion"
	];
	
	const baseUrl = 'https://www.portalinmobiliario.com';
	const suffix = '?ca=2&ts=1&mn=2&or=f-des&sf=0&sp=0&at=0&pg=1';

	const operation = getOperation();
	const region = getRegion();

	return propertyTypes.map(pt => {
		return `${baseUrl}/${operation}/${pt}/${region}${suffix}`;
	});
}

const getOperation = () => {
	const d = new Date();
	const min = d.getMinutes();
	return (min % 2 === 0) ? "venta" : "arriendo";
}

const getRegion = () => {
	const d = new Date();
	const min = d.getMinutes();

	const regions = [
		"metropolitana",
		"antofagasta",
		"arica-y-parinacota",
		"atacama",
		"aysen",
		"bernardo-ohiggins",
		"coquimbo",
		"la-araucania",
		"los-lagos",
		"de-los-rios",
		"magallanes-y-antartica-chilena",
		"maule",
		"valparaiso",
		"tarapaca",
		"biobio"
	];

	const index = (min % 15);
	return regions[index];
}