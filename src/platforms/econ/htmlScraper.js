import request from 'request';
import cheerio from 'cheerio';

import { 
    stripBlanks, 
    stripTabs, 
    stripFirstCharacter
} from '../../helpers/string.js';

import { parsePrice } from '../../helpers/price.js';

import { addToProperty } from '../../helpers/property.js';

import { getText, elExists } from '../../parser/index.js';

const arg = process.argv;
const BASE_URL = "http://www.economicos.cl";
let _id = arg[2];
let url = arg[3];

if (url === undefined){
    url = "http://www.economicos.cl/propiedades/hermosos-departamentos-mas-bodegas-a-cuadras-av-matta-codAAKMOCA.html";
} else {
    url = BASE_URL + url;
}

request(url, (err, res, html) => {
    /* Property starts as empty object */

    const $ = cheerio.load(html);

    let property = {};

    /* Basic selectors that are easily scrapable */
    const simpleSelectors = {
        title: 'div.cont_tit_detalle_f h1',
        propertyType: '#specs ul li:nth-of-type(1)',
        description: '#description p',
        operationType: '#specs ul li:nth-of-type(2)',
        date: '#specs ul li:nth-last-of-type(1)',
        county: '#specs ul li:nth-last-of-type(2)',
        region: '#specs ul li:nth-last-of-type(3)',
        price: '.cont_price_detalle_f',
        contactName: '.cont_datos_anunciante .vendor .cont_ecn_name_vendor',
        phoneNumber: '#phone',
        approximateLocation: 'div.cont_tit_detalle_f h3'
    }

    const complexSelectors = {
        details: '#specs'
    }

    for (let key in simpleSelectors){

        let selector = simpleSelectors[key];

        if (elExists($, selector)){
            /* Must access client global object as es6 does not support passing this */
            //const val = client.fetchText(selector); //fetches text from each html element
            const val = getText($, selector);
            let parsedVal = null;
            switch(key){
                case 'price':
                    parsedVal = parsePrice(val);
                    break;
                case 'title':
                case 'description':
                case 'approximateLocation':
                    parsedVal = val; //does nothing
                    break;
                case 'county':
                case 'region':
                    parsedVal = parseColonTupletKeepBlanks(val);
                    break;
                case 'date':
                    parsedVal = parseDate(val);
                    break;
                case 'operationType':
                case 'propertyType':
                    parsedVal = parseColonTupletDiscardBlanks(val);
                    break;
                case 'contactName':
                    parsedVal = parseContactName(val);
                    break;
                case 'phoneNumber':
                    parsedVal = parsePhoneNumber(val);
                    break;
            }
            property = addToProperty(property, key, parsedVal); //adds it to object
        }        
    }


    /* More difficult parsing */
    let detailsHtml = getText($, complexSelectors.details);
    let parsedHtml = stripTabs(detailsHtml);
    let detailsArr = parsedHtml.split("\n"); //transforms to array

    detailsArr = detailsArr.filter(el => { //discards fields that are duplicated or not needed
        const BLANK_LIST_FIELDS = ["Dormitorios", "Baños", "m2 construidos", "m2 terreno"];
        const length = BLANK_LIST_FIELDS.length;
        for (let i=0; i<length; i++){
            let field = BLANK_LIST_FIELDS[i];
            if (el.indexOf(field) > -1){
                return true;
            }
        }
        return false;
    });

    detailsArr.map(el => { /* string elements separated by colon */
        const [key, val] = el.split(":");
        const parsedVal = stripFirstCharacter(val);
        property = addToProperty(property, key, parsedVal);
    })

    //finally translate keys that are incorrectly named

    for (let key in property){
        const val = property[key];
        let newKey = null;

        const KEYS_TO_OVERWRITE = ["Dormitorios", "Baños", "m2 construidos", "m2 terreno"];

        if (KEYS_TO_OVERWRITE.indexOf(key) > -1){

            switch(key){
                case "Dormitorios":
                    newKey = "bedrooms";
                    break;
                case "Baños":
                    newKey = "bathrooms";
                    break;
                case "m2 construidos":
                    newKey = "smallSurface";
                    break;
                case "m2 terreno":
                    newKey = "bigSurface";
                    break;
            }

            property[newKey] = val;
            delete property[key];
        }
    }

    //returns to stdout
    console.log(JSON.stringify(property));
});


/* Str -> Str */
const parseDate = (str) => {
    const dateWithTime = parseColonTupletKeepBlanks(str);
    return dateWithTime.split(" ")[0];
}

/* Str -> Str */
const parsePhoneNumber = (str) => {
    return stripFirstCharacter(str);
}

/* Str -> Str */
const parseContactName = (str) => {
    const params = stripFirstCharacter(str).split(" ");
    const length = params.length;

    //length for one word 11
    //length for two words 12
    //and so on...

    //Must retrieve the length - 10 and stringify

    const elemsToSelect = length - 10;
    const name = params.slice(0, elemsToSelect).join(" ");
    return name;
}

/* Str -> Str */
const parseColonTupletKeepBlanks = (str) => {
    // must remove the first blank manually
    return stripFirstCharacter(str.split(":")[1]);
}

/* Str -> Str */
const parseColonTupletDiscardBlanks = (str) => {
    const val = str.split(":")[1];
    return stripBlanks(val);
}