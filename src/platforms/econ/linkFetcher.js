import request from 'request';
import cheerio from 'cheerio';

import { getLinks } from '../../parser/index.js';

import * as db from '../../db/index.js';

console.log("Running linkFetcher as ECON");

let url = 'http://www.economicos.cl/todo_chile/propiedades';

request(url, (err, res, html) => {
	let $ = cheerio.load(html);
	let links = getLinks(cheerio.load(html), '.results.left a');

	links = links.filter(link => { //removes waze links
		if (link.indexOf('waze') > -1){
			return false;
		}
		return true;
	});

	links = [ ...new Set(links)]; //removes duplicates

	let codes = links.map(link => {
		const regex = /cod(.*?)\.html$/gi;
	    const matches = regex.exec(link);
	    const code = matches[1];
	    return code;
	});	

	let listings = codes.map((e, i) => {
		let code = codes[i];
		let url = links[i];
		let platform = "ECON";
		return { code, url, platform };
	});

	if (listings.length > 0){
		/* Verify existing listings */
		/* Inserts new listings */

		db.findExistentAndInsertNew(listings).then(result => {
			console.log(`Inserted ${listings.length} records`);
		}).catch(err => {
			console.error(err);
		});
	}
});