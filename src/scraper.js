import * as db from './db/index.js';

import { exec } from 'child_process';

db.getLatest().then(listings => {
	listings.forEach(l => {
		const { platform, url, _id } = l;
		const execPath = getExecPathWithArguments(l);
		exec(execPath, (err, stdout, stderr) => {
			console.log(platform, url, _id);
			console.log("stdout", stdout);
			console.log("stderr", stderr.length);
			console.log("err", err);
			if (stdout.hasOwnProperty("error")){ /* handles yapo timeout with casper js */
				return;
			}

			if (err !== null || stderr.length > 0) {	
				console.error('error while executing', err, stderr);
				db.updateListing(_id, {listingStatus: false}).then(r => {
					console.log("Listing contained error, setting status as false");
				}).catch(err => {
					console.error("Error when updating listing", err);
				});
			} else {
				let listing = JSON.parse(stdout);
				if (!listing.hasOwnProperty("listingStatus")){ // comes as false wwith portal 404 - set true if there are no errors or if the field doesn't exist
					listing.listingStatus = true;
				}
				db.updateListing(_id, listing).then(r => {
					console.log("Listing successfully updated");
				}).catch(err => {
					console.error("Error when updating listing", err);
				});
			}
			return;
		});
	});
}).catch(err => {
	console.error(err);
});

const getExecPathWithArguments = (listing) => {
	const platforms = {
		ECON: 'econ',
		YAPO: 'yapo',
		PORTAL: 'portal'
	};
	const { platform, _id, url } = listing;
	let programToCall = null;
	if (platform === "ECON" || platform === "PORTAL"){
		programToCall = 'node';
	}

	if (platform === "YAPO"){
		programToCall = 'casperjs';
	}

	return `${programToCall} /home/dabao/scrapejs/dest/platforms/${platforms[platform]}/htmlScraper.js ${_id} ${url}`;
}