/* Obj, Str, Str -> Obj */
export const addToProperty = (property, key, val) => {
    property[key] = val;
    return property;
}