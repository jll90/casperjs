/* Str -> Str */
export const stripBlanks = str => {    
    return str.replace(/\s/g, '');
}

/* Str -> Str */
export const stripTabs = str => {
    return str.replace(/\t/g, '');
}

/* Str -> Str */
export const stripFirstCharacter = str => {
    return str.substring(1);
}

/* Str -> Str */
export const stripCarriageReturn = str => {
    return str.replace(/\n/g, '');
}

/* Str -> Str */
export const stripMultipleSpacesButEndings = str => {
	let newStr = str.replace(/\s\s+/g, ' ');
	return stripLastCharacter(stripFirstCharacter(newStr));
}

/* Str -> Str */
export const stripLastCharacter = str => {
	return str.slice(0, -1);
}

/* Str -> Str */
export const stripDoubleQuotes = str => {
	return str.replace(/\"/g, '');
}

/* Str -> Str */
export const stripBrackets = str => {
	return str.replace(/(\{|\})/g, '');
}

export const stripSemiColons = str => {
	return str.replace(/;/g, '');	
}

/* Str -> Str */
export const stripExcessBlanks = str => {
	return str.replace(/\s+/g, ' ').trim();
}

/* Str -> Str */
export const stripHtmlBlank = str => {
	return str.replace(/&nbsp/g, '').trim();
}