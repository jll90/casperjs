import numeral from 'numeral';

import { 
	stripTabs,
	stripCarriageReturn,
	stripBlanks } from './string.js';

numeral.register('locale', 'es', {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'k',
        million: 'mm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function (number) {
        var b = number % 10;
        return (b === 1 || b === 3) ? 'er' :
            (b === 2) ? 'do' :
            (b === 7 || b === 0) ? 'mo' :
    (b === 8) ? 'vo' :
    (b === 9) ? 'no' : 'to';
    },
    currency: {
        symbol: '$'
    }
});

numeral.locale('es');

/* Str -> Str */
export const parsePrice = str => {
    let newStr = stripTabs(str);
    newStr = stripDollarSign(newStr);
    newStr = stripCarriageReturn(newStr);
    newStr = stripBlanks(newStr);
    return numeral(newStr).value();
}

const stripDollarSign = str => {
    return str.replace(/\$/g, '');
}